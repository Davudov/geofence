package com.udacity.project4.authentication

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.firebase.ui.auth.AuthMethodPickerLayout
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.AuthUI.IdpConfig
import com.firebase.ui.auth.IdpResponse
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuth.AuthStateListener
import com.udacity.project4.R
import com.udacity.project4.locationreminders.RemindersActivity
import kotlinx.android.synthetic.main.activity_authentication.*
import timber.log.Timber


/**
 * This class should be the starting point of the app, It asks the users to sign in / register, and redirects the
 * signed in users to the RemindersActivity.
 */

class AuthenticationActivity : AppCompatActivity() {
    private lateinit var firebaseAuth:FirebaseAuth
    private lateinit var  mAuthListener:AuthStateListener
    private val RC_SIGN_IN = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)
//         TODO: Implement the create account and sign in using FirebaseUI, use sign in using email and sign in using Google

//          TODO: If the user was authenticated, send him to RemindersActivity

//          TODO: a bonus is to customize the sign in flow to look nice using :
        //https://github.com/firebase/FirebaseUI-Android/blob/master/auth/README.md#custom-layout

        login_btn.setOnClickListener {
            login()
        }

        firebaseAuth = FirebaseAuth.getInstance()

         mAuthListener = AuthStateListener {
            val user = FirebaseAuth.getInstance().currentUser
            if (user != null) {
                val intent = Intent(this, RemindersActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
    }
    override fun onStart() {
        super.onStart()
        firebaseAuth.addAuthStateListener(mAuthListener)
    }


    override fun onResume() {
        super.onResume()
        firebaseAuth.addAuthStateListener(mAuthListener)
    }


    override fun onStop() {
        super.onStop()
        firebaseAuth.removeAuthStateListener(mAuthListener)
    }
    private fun login() {

        val customLayout = AuthMethodPickerLayout.Builder(R.layout.layout_login)
            .setGoogleButtonId(R.id.login_with_google)
            .setEmailButtonId(R.id.login_with_email)
            .build()


        startActivityForResult(
            AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setIsSmartLockEnabled(false)
                .setAuthMethodPickerLayout(customLayout)
                .setAvailableProviders(
                    arrayListOf(
                        IdpConfig.GoogleBuilder()
                            .build(),
                        IdpConfig.EmailBuilder()
                            .build()
                    )
                )
                .build(), RC_SIGN_IN
        )

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val response: IdpResponse? = IdpResponse.fromResultIntent(data);
            Timber.d("RESULT_OK1 ${response?.email}")
            if (resultCode == RESULT_OK) {
                Timber.d("RESULT_OK2 ${response?.email}")
            } else {

            }
        }
    }


}
