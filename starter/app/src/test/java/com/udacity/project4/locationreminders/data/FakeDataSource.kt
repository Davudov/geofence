package com.udacity.project4.locationreminders.data

import com.udacity.project4.locationreminders.data.dto.ReminderDTO
import com.udacity.project4.locationreminders.data.dto.Result

//Use FakeDataSource that acts as a test double to the LocalDataSource
class FakeDataSource : ReminderDataSource {
    private var shouldReturnError = false

    fun setReturnError(value: Boolean) {
        shouldReturnError = value
    }

    override suspend fun getReminders(): Result<List<ReminderDTO>> {
        return if (shouldReturnError) {
            return Result.Error("Test exception")
        } else
            Result.Success(
                    listOf(
                            ReminderDTO(
                                    "title",
                                    "descr",
                                    "Baku",
                                    123213.213,
                                    123123.12,
                                    "1231kmrim3ek"
                            )
                    )
            )
    }

    override suspend fun saveReminder(reminder: ReminderDTO) {

    }

    override suspend fun getReminder(id: String): Result<ReminderDTO> {
        return Result.Success(
                ReminderDTO(
                        "title",
                        "descr",
                        "Baku",
                        123213.213,
                        123123.12,
                        "1231kmrim3ek"
                )
        )
    }

    override suspend fun deleteAllReminders() {

    }


}