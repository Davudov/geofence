package com.udacity.project4

import android.app.Application
import android.widget.Toast
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Root
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.udacity.project4.locationreminders.RemindersActivity
import com.udacity.project4.locationreminders.data.ReminderDataSource
import com.udacity.project4.locationreminders.data.local.LocalDB
import com.udacity.project4.locationreminders.data.local.RemindersLocalRepository
import com.udacity.project4.locationreminders.reminderslist.RemindersListViewModel
import com.udacity.project4.locationreminders.savereminder.SaveReminderViewModel
import com.udacity.project4.util.ToastMatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.hamcrest.Matcher
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.AutoCloseKoinTest
import org.koin.test.get


@SmallTest
@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
//END TO END test to black box test the app
class RemindersActivityTest :
    AutoCloseKoinTest() {

//    @Rule
//    var activityTestRule: ActivityScenarioRule<RemindersActivity>? =
//        ActivityScenarioRule<RemindersActivity>(RemindersActivity::class.java)

    @get:Rule
    var activityRule: ActivityScenarioRule<RemindersActivity> =
        ActivityScenarioRule<RemindersActivity>(RemindersActivity::class.java)


    @Test
    fun checking_toastmessage_test() {

        var activity: RemindersActivity? = null

        activityRule.scenario.onActivity {
            activity = it
        }

        activity?.runOnUiThread {
            Toast.makeText(activity, "Test toast", Toast.LENGTH_SHORT).show()
        }

        onView(withText("Test toast")).inRoot(isToast()).check(matches(isDisplayed()))

    }


    fun isToast(): Matcher<Root?> {
        return ToastMatcher()
    }

    private lateinit var repository: ReminderDataSource
    private lateinit var appContext: Application

    /**
     * As we use Koin as a Service Locator Library to develop our code, we'll also use Koin to test our code.
     * at this step we will initialize Koin related code to be able to use it in out testing.
     */
    @Before
    fun init() {
        stopKoin()//stop the original app koin
        appContext = getApplicationContext()
        val myModule = module {
            viewModel {
                RemindersListViewModel(
                    appContext,
                    get() as ReminderDataSource
                )
            }
            single {
                SaveReminderViewModel(
                    appContext,
                    get() as ReminderDataSource
                )
            }
            single { RemindersLocalRepository(get()) as ReminderDataSource }
            single { LocalDB.createRemindersDao(appContext) }
        }
        //declare a new koin module
        startKoin {
            modules(listOf(myModule))
        }
        //Get our real repository
        repository = get()

        //clear the data to start fresh
        runBlocking {
            repository.deleteAllReminders()
        }
    }



}
