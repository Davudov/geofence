package com.udacity.project4.locationreminders.data.local

import androidx.test.core.app.ApplicationProvider
import androidx.test.filters.MediumTest
import androidx.test.runner.AndroidJUnit4
import com.udacity.project4.locationreminders.data.dto.ReminderDTO
import com.udacity.project4.locationreminders.data.dto.Result
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers
import org.junit.*
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
//Medium Test to test the repository
@MediumTest
class RemindersLocalRepositoryTest {
    lateinit var repo: RemindersLocalRepository

    @Before
    fun initBefore() {
        val remindersDao = LocalDB.createRemindersDao(ApplicationProvider.getApplicationContext())
        repo = RemindersLocalRepository(remindersDao)
    }


    @Test
    fun should_Insert_Item() {
        val data = ReminderDTO(
            "title",
            "descr",
            "Baku",
            123213.213,
            123123.12,
            "1231kmrim3ek"
        )
        runBlocking {
            repo.saveReminder(data)
        }

        runBlocking {
            val dataTest = repo.getReminder(data.id)
            assertThat(dataTest is Result.Success, Matchers.`is`(true))
        }
    }

    @Test
    fun saveReminders() {
        runBlocking {
            repo.deleteAllReminders()
        }
        runBlocking {
            val data = repo.getReminders()
            if (data is Result.Success)
            Assert.assertEquals(data.data.size, 0)
        }
    }
}