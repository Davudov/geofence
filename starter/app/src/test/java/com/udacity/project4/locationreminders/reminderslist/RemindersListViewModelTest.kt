package com.udacity.project4.locationreminders.reminderslist

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.udacity.project4.locationreminders.data.FakeDataSource
import com.udacity.project4.locationreminders.data.dto.ReminderDTO
import com.udacity.project4.locationreminders.data.dto.Result
import com.udacity.project4.utils.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config


@Config(sdk = [Build.VERSION_CODES.P])
@RunWith(AndroidJUnit4::class)
@ExperimentalCoroutinesApi
class RemindersListViewModelTest {
    //

    private val fakeDataSource = FakeDataSource()

    @get:Rule
    var instantExecuteRule = InstantTaskExecutorRule()
    private lateinit var viewModel: RemindersListViewModel

    @Before
    fun setUp() {
        viewModel = RemindersListViewModel(
                ApplicationProvider.getApplicationContext(), fakeDataSource
        )
    }


    @Test
    fun getRemindersList() = runBlocking {
        val result = viewModel.dataSource.getReminders()
        when (result) {
            is Result.Success<*> -> {
                val dataList = ArrayList<ReminderDataItem>()
                dataList.addAll((result.data as List<ReminderDTO>).map { reminder ->
                    //map the reminder data from the DB to the be ready to be displayed on the UI
                    ReminderDataItem(
                            reminder.title,
                            reminder.description,
                            reminder.location,
                            reminder.latitude,
                            reminder.longitude,
                            reminder.id
                    )
                })
                viewModel.remindersList.value = dataList
            }
        }
        val value = viewModel.remindersList.getOrAwaitValue()
        assertThat(value.isNotEmpty(), `is`(true))

    }

    @Test
    fun shouldReturnError()= runBlocking{
        fakeDataSource.setReturnError(true)
        val result = viewModel.dataSource.getReminders()
        when (result) {
            is Result.Success<*> -> {
                val dataList = ArrayList<ReminderDataItem>()
                dataList.addAll((result.data as List<ReminderDTO>).map { reminder ->
                    //map the reminder data from the DB to the be ready to be displayed on the UI
                    ReminderDataItem(
                            reminder.title,
                            reminder.description,
                            reminder.location,
                            reminder.latitude,
                            reminder.longitude,
                            reminder.id
                    )
                })
                viewModel.remindersList.value = dataList
            }
        }
        assertThat(viewModel.empty.getOrAwaitValue(), `is`(true))
        assertThat(viewModel.error.getOrAwaitValue(), `is`(true))
    }

    @Test
    fun check_loading() {
        viewModel.showLoading.postValue(true)
        val value = viewModel.showLoading.getOrAwaitValue()
        assertThat(value, `is`(true))
    }


}