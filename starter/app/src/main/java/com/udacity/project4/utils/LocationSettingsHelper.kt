package com.udacity.project4.utils
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.Looper
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.Task
import timber.log.Timber


@SuppressLint("MissingPermission")
class LocationSettingsHelper(private val context: Context, private val activity: Activity, private val listener: SettingsListener) {
    private var mFusedLocationProviderClient: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)

    var currentLocation = MutableLiveData<LatLng>()

    private val locationRequest: LocationRequest = LocationRequest.create().apply {
        interval = 5000
        priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
    }

    companion object {
        const val REQUEST_SETTINGS = 1
    }


    init {
        checkSettings()
        listenLocationAvailability()
    }

    private fun checkSettings() {


        val builder = LocationSettingsRequest.Builder().apply { addLocationRequest(locationRequest) }

        val client: SettingsClient = LocationServices.getSettingsClient(context)

        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())

        task.addOnSuccessListener(activity) {
            Timber.d("All location settings are satisfied. ")
            listener.onSettingsSatisfied()
        }
        task.addOnFailureListener(activity) { e ->
            if (e is ResolvableApiException)
                listener.onResolvableApiException(e)
        }
    }

    private fun listenLocationAvailability() {
        mFusedLocationProviderClient.requestLocationUpdates(locationRequest, object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                currentLocation.postValue(LatLng(locationResult.lastLocation.latitude, locationResult.lastLocation.longitude))
                Timber.d(locationResult.toString())
            }

            override fun onLocationAvailability(locationAvailability: LocationAvailability) {
                Timber.d("%s", locationAvailability)
                if (currentLocation.value == null)
                    listener.onLocationAvailable(locationAvailability.isLocationAvailable)
            }
        }, Looper.getMainLooper())
    }


    fun refreshLocation() {
        checkSettings()
    }

}

interface SettingsListener {
    fun onResolvableApiException(e: ResolvableApiException)
    fun onSettingsSatisfied()
    fun onLocationAvailable(available: Boolean)
}
