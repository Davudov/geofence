package com.udacity.project4.locationreminders.data.local

import androidx.room.Room
import androidx.test.InstrumentationRegistry.getContext
import androidx.test.core.app.ApplicationProvider
import androidx.test.filters.SmallTest;
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import com.udacity.project4.locationreminders.data.dto.ReminderDTO

import org.junit.runner.RunWith;

import kotlinx.coroutines.ExperimentalCoroutinesApi;
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.*
import java.security.AccessController.getContext

@ExperimentalCoroutinesApi
@RunWith(androidx.test.ext.junit.runners.AndroidJUnit4::class)
//Unit test the DAO
@SmallTest
class RemindersDaoTest {
    private lateinit var remindersDao: RemindersDao

    @Before
    fun initDb() {
       remindersDao = LocalDB.createRemindersDao(ApplicationProvider.getApplicationContext())
    }


    @Test
    fun should_Insert_Item() {
        val data =  ReminderDTO(
            "title",
            "descr",
            "Baku",
            123213.213,
            123123.12,
            "1231kmrim3ek"
        )
        runBlocking {
            remindersDao.saveReminder(data)
        }

        runBlocking {
            val dataTest = remindersDao.getReminderById(data.id)
            Assert.assertEquals(data.id, dataTest?.id)
        }
    }

    @Test
    fun saveReminders(){
        runBlocking {
            remindersDao.deleteAllReminders()
        }
        runBlocking {
            val data = remindersDao.getReminders()
            Assert.assertEquals(data.size,0)
        }
    }
}