package com.udacity.project4.locationreminders.savereminder

import android.os.Build
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.udacity.project4.locationreminders.data.FakeDataSource
import com.udacity.project4.locationreminders.data.dto.ReminderDTO
import com.udacity.project4.locationreminders.data.dto.Result
import com.udacity.project4.utils.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import java.util.*

@Config(sdk = [Build.VERSION_CODES.P])
@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class SaveReminderViewModelTest {

    //    @get:Rule
//    var instantExecuteRule = InstantTaskExecutorRule()
    private lateinit var viewModel: SaveReminderViewModel
    private val fakeDataSource = FakeDataSource()

    @Before
    fun setUp() {
        viewModel = SaveReminderViewModel(
                ApplicationProvider.getApplicationContext(), fakeDataSource
        )
    }

    @Test
    fun saveReminder() = runBlocking {
        val id = UUID.randomUUID().toString()
        val reminderData = ReminderDTO("title", "description", "title", 123.2, 12321.23, id)
        viewModel.dataSource.saveReminder(
                ReminderDTO(
                        reminderData.title,
                        reminderData.description,
                        reminderData.location,
                        reminderData.latitude,
                        reminderData.longitude,
                        reminderData.id
                )
        )
        val savedReminder = viewModel.dataSource.getReminder(id)
        assert(savedReminder is Result.Success)
        viewModel.onClear()
        assert(viewModel.reminderTitle.value == null)
        assert(viewModel.reminderDescription.value == null)
        assert(viewModel.reminderSelectedLocationStr.value == null)
        assert(viewModel.selectedPOI.value == null)
        assert(viewModel.longitude.value == null)
        assert(viewModel.latitude.value == null)
    }

    @Test
    fun check_loading() {
        viewModel.showLoading.postValue(true)
        val value = viewModel.showLoading.getOrAwaitValue()
        Assert.assertThat(value, CoreMatchers.`is`(true))
    }
}