package com.udacity.project4.locationreminders.savereminder.selectreminderlocation


import android.Manifest
import android.content.IntentSender.SendIntentException
import android.content.pm.PackageManager
import android.content.res.Resources
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.os.Looper
import android.view.*
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.udacity.project4.R
import com.udacity.project4.base.BaseFragment
import com.udacity.project4.databinding.FragmentSelectLocationBinding
import com.udacity.project4.locationreminders.savereminder.SaveReminderViewModel
import com.udacity.project4.utils.LocationSettingsHelper
import com.udacity.project4.utils.LocationSettingsHelper.Companion.REQUEST_SETTINGS
import com.udacity.project4.utils.SettingsListener
import com.udacity.project4.utils.setDisplayHomeAsUpEnabled
import kotlinx.android.synthetic.main.fragment_select_location.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.android.ext.android.inject
import timber.log.Timber
import java.io.IOException


class SelectLocationFragment : OnMapReadyCallback, GoogleMap.OnMapLoadedCallback,
        GoogleMap.OnCameraIdleListener, SettingsListener, BaseFragment(),
    GoogleMap.OnMapClickListener {
    private val requestCode = 101

    //Use Koin to get the view model of the SaveReminder
    override val _viewModel: SaveReminderViewModel by inject()
    private lateinit var binding: FragmentSelectLocationBinding
    private var currentLocation: LatLng? = null
    private lateinit var map: SupportMapFragment
    private var googleMap: GoogleMap? = null
    private var geocoder: Geocoder? = null
    private lateinit var mFusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback
    private lateinit var locationSettingsHelper: LocationSettingsHelper


    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding =
                DataBindingUtil.inflate(inflater, R.layout.fragment_select_location, container, false)

        binding.viewModel = _viewModel
        binding.lifecycleOwner = this

        setHasOptionsMenu(true)
        setDisplayHomeAsUpEnabled(true)

//        TODO: add the map setup implementation
//        TODO: zoom to the user location after taking his permission
//        TODO: add style to the map
//        TODO: put a marker to location that the user selected


//        TODO: call this function after the user confirms on the selected location
        onLocationSelected()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _viewModel.actions.observe(viewLifecycleOwner, Observer {
            if (it == SaveReminderViewModel.Actions.ZOOM_CURRENT) {
                googleMap ?: return@Observer
                currentLocation ?: return@Observer
                animate()
            }
        })

        val permissions = arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
        )

        mFusedLocationProviderClient =
                LocationServices.getFusedLocationProviderClient(requireContext())
        if (ActivityCompat.checkSelfPermission(
                        requireContext(),
                        Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        requireContext(),
                        Manifest.permission.ACCESS_COARSE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
        ) {
            initMap()
        } else
            requestPermissions(permissions, requestCode)

        save.setOnClickListener {
            findNavController().navigateUp()
        }



        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                val receivedLocation = LatLng(
                        locationResult.lastLocation.latitude,
                        locationResult.lastLocation.longitude
                )

                Timber.v("Location Result: $locationResult")

                _viewModel.gpsValue.complete(receivedLocation)

                Timber.d("Removing location updates")

                mFusedLocationProviderClient.removeLocationUpdates(this)
            }
        }

        /**start request for location*/
        startForLocationServices()
    }


    private fun onLocationSelected() {
        //        TODO: When the user confirms on the selected location,
        //         send back the selected location details to the view model
        //         and navigate back to the previous fragment to save the reminder and add the geofence
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.map_options, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        // TODO: Change the map type based on the user's selection.
        R.id.normal_map -> {
            googleMap?.mapType = GoogleMap.MAP_TYPE_NORMAL;
            true
        }
        R.id.hybrid_map -> {
            googleMap?.mapType = GoogleMap.MAP_TYPE_HYBRID;
            true
        }
        R.id.satellite_map -> {
            googleMap?.mapType = GoogleMap.MAP_TYPE_SATELLITE;
            true
        }
        R.id.terrain_map -> {
            googleMap?.mapType = GoogleMap.MAP_TYPE_TERRAIN;
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun animate() {
        googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 15f))
    }


    override fun onCameraIdle() {

    }


    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == this.requestCode) {
            if (grantResults.isNotEmpty()
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                initMap()
            } else {

                // permission denied, boo! Disable the
                // functionality that depends on this permission.
                Toast.makeText(
                        requireContext(),
                        "Permission denied",
                        Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun startLocation() {
        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                val latLng = _viewModel.gpsValue.await()
                _viewModel.latitude.value = latLng.latitude
                _viewModel.longitude.value = latLng.longitude
                latLng.longitude.let { setMarkerPlace(it, latLng.latitude) }
                googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(latLng.latitude, latLng.longitude), 13f))
            }
        }
        locationSettingsHelper = LocationSettingsHelper(requireContext(), requireActivity(), this)
//        locationSettingsHelper.currentLocation.observe(viewLifecycleOwner, Observer { latLng: LatLng? ->
//            _viewModel.latitude.value = latLng?.latitude
//            _viewModel.longitude.value = latLng?.longitude
//            latLng?.longitude?.let { setMarkerPlace(it, latLng.latitude) }
//            if (latLng != null) googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(latLng.latitude, latLng.longitude), 13f))
//        })

    }

    private fun initMap() {
        map = (childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment)
        map.getMapAsync(this)
    }

    override fun onMapReady(p0: GoogleMap?) {
        p0 ?: return
        geocoder = Geocoder(requireContext())
        googleMap = p0
        if (ActivityCompat.checkSelfPermission(
                        requireContext(),
                        Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        requireContext(),
                        Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        googleMap?.setOnMapClickListener(this)
        googleMap?.isMyLocationEnabled = true
        googleMap?.setOnMapLoadedCallback(this)
        googleMap?.uiSettings?.isRotateGesturesEnabled = true
        googleMap?.uiSettings?.isCompassEnabled = false
        googleMap?.uiSettings?.isTiltGesturesEnabled = false
        googleMap?.uiSettings?.isMapToolbarEnabled = false
        googleMap?.uiSettings?.isZoomControlsEnabled = true
        googleMap?.uiSettings?.isMyLocationButtonEnabled = true

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            val success = googleMap?.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            requireContext(), R.raw.style_json
                    )
            )!!
            if (!success) {
                Timber.e("Style parsing failed.")
            }
        } catch (e: Resources.NotFoundException) {
            Timber.e("Can't find style. Error: ")
        }
    }


    private fun setMarkerPlace(longitude: Double, latitude: Double) {
        geocoder ?: return
        try {
            val address: List<Address> = geocoder?.getFromLocation(
                    latitude,
                    longitude,
                    1
            ) as List<Address>
            address ?: return
            if (address.isEmpty()) return
            else {
                _viewModel.reminderSelectedLocationStr.value = MarkerOptions().title(address[0].getAddressLine(0)).title
                val marker = MarkerOptions().position(LatLng(latitude, longitude)).title(address[0].getAddressLine(0))
                googleMap?.clear()
                googleMap?.addMarker(marker)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }


    override fun onLowMemory() {
        super.onLowMemory()
        map.onLowMemory()
    }

    override fun onDetach() {
        super.onDetach()
        map.onDetach()
    }

    override fun onMapLoaded() {
        startLocation()
    }

    private fun startForLocationServices() {
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireContext())
        if (ActivityCompat.checkSelfPermission(
                        requireContext(),
                        Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        requireContext(),
                        Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                    arrayOf(
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                    ), requestCode
            )
            return
        }
        mFusedLocationProviderClient.requestLocationUpdates(
                createLocationRequest(),
                locationCallback, Looper.getMainLooper()
        )
    }


    private fun createLocationRequest() =
            LocationRequest.create()?.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                    ?.setInterval(50_000L)

    override fun onResolvableApiException(e: ResolvableApiException) {
// Location settings are not satisfied, but this can be fixed
        // by showing the user a dialog.
        // Location settings are not satisfied, but this can be fixed
        // by showing the user a dialog.
        try {
            // Show the dialog by calling startResolutionForResult(),
            // and check the result in onActivityResult().
            startIntentSenderForResult(e.resolution.intentSender, REQUEST_SETTINGS, null, 0, 0, 0, null)
        } catch (sendEx: SendIntentException) {
            // Ignore the error.
            Timber.e(sendEx)
        }

    }

    override fun onSettingsSatisfied() {

    }

    override fun onLocationAvailable(available: Boolean) {

    }

    override fun onMapClick(p0: LatLng?) {
        googleMap?.clear()
        p0?.longitude?.let { setMarkerPlace(it, p0.latitude) }
    }

}
