package com.udacity.project4.locationreminders.reminderslist

import android.app.Activity
import android.widget.Toast
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.fragment.app.testing.withFragment
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.google.android.material.snackbar.Snackbar
import com.udacity.project4.R
import com.udacity.project4.locationreminders.data.FakeDataSourceUI
import com.udacity.project4.locationreminders.data.dto.ReminderDTO
import com.udacity.project4.locationreminders.data.dto.Result
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.hamcrest.Matchers.not
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.mock


@RunWith(AndroidJUnit4::class)
@ExperimentalCoroutinesApi
//UI Testing
@MediumTest
class ReminderListFragmentTest {

    @get:Rule
    var instantExecuteRule = InstantTaskExecutorRule()
    private lateinit var viewModel: RemindersListViewModel

    private var dataSourceUI = FakeDataSourceUI()

    @Before
    fun setViewModel() {
        viewModel =
            RemindersListViewModel(ApplicationProvider.getApplicationContext(), dataSourceUI)
    }

    @Test
    fun reminderListFragment_Initial_Test() {
//        GIVEN
        val scenario = launchFragmentInContainer<ReminderListFragment>(null, R.style.AppTheme)
        val navController = mock(NavController::class.java)
        scenario.withFragment {
            this.view?.let { it1 -> Navigation.setViewNavController(it1, navController) }
            this
        }
        //WHEN CLICK ADD BUTTON
        Espresso.onView(withId(R.id.addReminderFAB)).perform(ViewActions.click())

        //THEN - verify that we navigate to the save reminder screen
        Mockito.verify(navController).navigate(ReminderListFragmentDirections.toSaveReminder())
    }

    @Test
    fun snackBarTest() {
        val scenario = launchFragmentInContainer<ReminderListFragment>(null, R.style.AppTheme)
        viewModel.dataSource = object : FakeDataSourceUI() {
            override suspend fun getReminders(): Result<List<ReminderDTO>> {
                return Result.Error("Snackbar testing")
            }
        }

        viewModel.loadReminders()

        scenario.withFragment {
            viewModel.showSnackBar.observe(this, Observer {
                Snackbar.make(this.view!!, it, Snackbar.LENGTH_LONG).show()
            })
        }
        onView(withId(com.google.android.material.R.id.snackbar_text))
            .check(matches(isDisplayed()))
    }


    @Test
    fun statisticsFragmentTest() {
        val scenario = launchFragmentInContainer<ReminderListFragment>()
        scenario.recreate()
    }
}

class ToastPopup(activity: Activity) {
    var oopsToast: ViewInteraction

    init {
        oopsToast = onView(withText("Test toast"))
            .inRoot(withDecorView(not(activity.window.decorView)))
    }
}